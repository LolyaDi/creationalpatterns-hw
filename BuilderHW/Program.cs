﻿using System;
using System.Collections.Generic;

namespace BuilderHW
{
    public interface IComplectation
    {
        void GetMainboard();
        void GetProcessor();
        void GetVideoCard();
        void GetSoundCard();
        void GetTuner();
        Product GetProduct();
    }

    public class Complectation : IComplectation
    {
        private Product _product;

        public Complectation()
        {
            Reset();
        }

        private void Reset()
        {
            _product = new Product();
        }

        public void GetMainboard()
        {
            _product.Add("\"Mainboard\"");
        }

        public void GetProcessor()
        {
            _product.Add("\"Processor\"");
        }

        public void GetSoundCard()
        {
            _product.Add("\"Sound Card\"");
        }

        public void GetTuner()
        {
            _product.Add("\"Tuner\"");
        }

        public void GetVideoCard()
        {
            _product.Add("\"Video Card\"");
        }

        public Product GetProduct()
        {
            var product = _product;

            Reset();

            return product;
        }
    }

    public class Product
    {
        private List<string> _parts;

        public Product()
        {
            _parts = new List<string>();
        }

        public void Add(string part)
        {
            _parts.Add(part);
        }

        public string GetParts()
        {
            string text = "";

            for (int i = 0; i < _parts.Count; i++)
            {
                text += $"{_parts[i]}, ";
            }

            return $"Product parts: {text.Remove(text.Length - 2)}\n";
        }
    }

    public abstract class Manufacturer
    {
        public IComplectation Complectation { get; protected set; }

        public Manufacturer()
        {
            Complectation = new Complectation();
        }

        public void Basic()
        {
            Complectation.GetMainboard();
            Complectation.GetProcessor();
        }

        public void Standart()
        {
            Complectation.GetMainboard();
            Complectation.GetProcessor();
            Complectation.GetVideoCard();
        }

        public void StandartPlus()
        {
            Complectation.GetMainboard();
            Complectation.GetProcessor();
            Complectation.GetVideoCard();
            Complectation.GetSoundCard();
        }

        public void Lux()
        {
            Complectation.GetMainboard();
            Complectation.GetProcessor();
            Complectation.GetVideoCard();
            Complectation.GetSoundCard();
            Complectation.GetTuner();
        }
    }

    public class Dell: Manufacturer
    {
    }

    public class Sony: Manufacturer
    {
    }

    public class Program
    {
        public static void Main(string[] args)
        {
            var dellManufacturer = new Dell();

            dellManufacturer.Lux();

            Console.WriteLine(dellManufacturer.Complectation.GetProduct().GetParts());

            Console.ReadLine();
        }
    }
}
