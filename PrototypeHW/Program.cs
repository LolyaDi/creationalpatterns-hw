﻿using System;

public interface IClone<T>: ICloneable where T: class
{
    T DeepCopy();
}

public class Person: IClone<Person>
{
    public Id _id;
    public string _name;
    public string _secondName;
    public int _age;
    public DateTime _birthDate;
    public Address _address;

    public object Clone()
    {
        return MemberwiseClone() as Person;
    }
    
    public Person DeepCopy()
    {
        var clone = MemberwiseClone() as Person;
        
        clone._id = new Id(_id._id);
        clone._name = string.Copy(_name);
        clone._address = _address;

        return clone;
    }
}

public class Address
{
    public string _street;
    public int _number;

    public Address(string street, int number)
    {
        _street = street;
        _number = number;
    }
}

public class Id
{
    public int _id;

    public Id(int id)
    {
        _id = id;
    }
}

public class Program
{
    public static void Main(string[] args)
    {
        var firstPerson = new Person
        {
            _id = new Id(666),
            _name = "Jack",
            _secondName = "Daniels",
            _age = 42,
            _birthDate = Convert.ToDateTime("1977-01-01"),
            _address = new Address("Kings Road", 15)
        };

        var secondPerson = firstPerson.Clone() as Person;
        var thirdPerson = firstPerson.DeepCopy();

        Console.WriteLine("Original values of first, second, third people:");

        Console.WriteLine("\tFirst person instance values:");
        Print(firstPerson);

        Console.WriteLine("\n\tSecond person instance values:");
        Print(secondPerson);

        Console.WriteLine("\n\tThird person instance values:");
        Print(thirdPerson);

        firstPerson._id._id = 7878;
        firstPerson._name = "Frank";
        firstPerson._secondName = "Simpson";
        firstPerson._age = 32;
        firstPerson._birthDate = Convert.ToDateTime("1900-01-01");
        firstPerson._address = new Address("Wall street", 10);

        secondPerson._address = firstPerson._address;
        
        Console.WriteLine("\nValues of first, second and third people after changes to first one:");

        Console.WriteLine("\tFirst person instance values:");
        Print(firstPerson);

        Console.WriteLine("\n\tSecond person instance values (reference values have changed):");
        Print(secondPerson);

        Console.WriteLine("\n\tThird person instance values (everything was kept the same):");
        Print(thirdPerson);

        Console.ReadLine();
    }

    public static void Print(Person person)
    {
        Console.WriteLine("\tId: #{0:d}", person._id._id);
        Console.WriteLine("\tName: {0:s}, Second Name: {1:s}, Age: {2:d}, BirthDate: {3:MM/dd/yy}",
            person._name, person._secondName, person._age, person._birthDate);
        Console.WriteLine("\tAddress: {0:d} {1:s}", person._address._number, person._address._street);
    }
}